package com.yoelestrada.springboot.backend.apirest.models.services;

import com.yoelestrada.springboot.backend.apirest.models.entities.Client;

import java.util.List;

public interface IClientService {

    // Devuelve la totalidad de los clientes
    public List<Client> findAll();
    // Ahora vamos con el crud de cliente.

    // Devuelve el cliente elegido mediante su id
    public Client findById(Long id);
    // Guarda el cliente en la Base de datos
    public Client save(Client client);
    // Elimina el cliente de la base de datos mediante su id
    public void delete(Long id);
}
