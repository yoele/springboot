package com.yoelestrada.springboot.backend.apirest.models.services;

import com.yoelestrada.springboot.backend.apirest.models.dao.IClientDao;
import com.yoelestrada.springboot.backend.apirest.models.entities.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientServiceImpl implements IClientService {

    @Autowired
    private IClientDao clientDao;

    @Override
    @Transactional(readOnly = true) //Se puede omitir porque CrudRepository ya viene con trasactional
    public List<Client> findAll() {
        return (List<Client>) clientDao.findAll();
    }

    @Override
    @Transactional(readOnly = true) // Como es un metodo custom tenemos que ponerlo en trasactional
    public Client findById(Long id) {
        return clientDao.findById(id).orElse(null);
    }

    @Override
    @Transactional // Obtiene el acceso completo ya que necesita rw
    public Client save(Client client) {
        return clientDao.save(client);
    }

    @Override
    @Transactional // Obtiene el acceso completo ya que necesita rw
    public void delete(Long id) {
        clientDao.deleteById(id);
    }

}
