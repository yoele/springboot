package com.yoelestrada.springboot.backend.apirest.models.dao;

import com.yoelestrada.springboot.backend.apirest.models.entities.Client;
import org.springframework.data.repository.CrudRepository;

public interface IClientDao extends CrudRepository<Client, Long> {
    //Para los métodos propios hay que utilizar el @Transactional
}
