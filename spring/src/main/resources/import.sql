INSERT INTO clients (name, last_name, created_at, email) VALUES ('Yoel', 'Estrada', '2020-05-25', 'yoel@estrada.com');
INSERT INTO clients (name, last_name, created_at, email) VALUES ('Tomas', 'Toloza', '2019-03-22', 'tomas@toloza.com');
INSERT INTO clients (name, last_name, created_at, email) VALUES ('Dario', 'Gomez', '2020-01-13', 'dario@gomez.com');
INSERT INTO clients (name, last_name, created_at, email) VALUES ('Matias', 'Katz', '2018-10-23', 'matias@katz.com');
INSERT INTO clients (name, last_name, created_at, email) VALUES ('Hernan', 'Nagorny', '2020-4-09', 'hernan@nagorny.com');